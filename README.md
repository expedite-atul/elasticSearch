#	  elasticSearch & Kibana initialization
# 	  Install and setup elasticSearch & Kibana in Ubuntu 18.04
## 	  Steps to initialize the setup
###   Open Terminal (ctrl+alt+t)

####  Install and setup java
     	
	- sudo add-apt-repository ppa:webupd8team/java
	- sudo apt update 
	- sudo apt install oracle-java8-installer //select yes on prompt
	
    - sudo apt install oracle-java8-set-default //this will set the path automatically
	
	- javac -version 

###    Download and Install elasticsearch.tar.gz manually follow the links and instructions

####    Download tar.gz from || Remember use the same version of Elasticsearch & Kibana
       	 
	 || https://www.elastic.co/start?iesrc=ctr

####   OR Download directly from these two links for version 6.5.4
       
       || https://artifacts.elastic.co/downloads/kibana/kibana-6.5.4-linux-x86_64.tar.gz
       || https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.4.tar.gz

####   Extract files using 
         - tar -xvf <filename.tar.gz>
         - cd elasticsearch-6.5.4
         - ./bin/elasticsearch

###### It will start the elasticsearch

####   Open another terminal and repeat the same for kibana 
         - tar -xvf <filename.tar.gz>
         - cd kibana-6.5.4
         - ./bin/kibana

####   After kibana has started succesfully
         > open Browser terminal and move to the given default port 
         || http://localhost:5601
